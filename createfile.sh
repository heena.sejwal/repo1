#! /bin/bash

File=$1
echo "checking a file $File "
	if [ -e $File ]; then
		echo "File $File already exists"
	else
		echo "creating file $File"
		touch $File
	fi
